import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-total-pedido',
  template: `
    <div class="form-row">
      <div class="form-group col d-flex justify-content-end">
        <h4>Total do pedido: {{ total | currency:'BRL' }}</h4>
      </div>
    </div>
  `,
})
export class TotalPedidoComponent {
  @Input() total = 0.0;
  @Input() products: any[] = [];


}
